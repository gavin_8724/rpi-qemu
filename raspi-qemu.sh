#!/bin/bash -ex

#Purpose: Download and prepare Raspberry Pi emulation with QEMU
img_source="http://files.velocix.com/c1410/images/raspbian/2012-12-16-wheezy-raspbian/2012-12-16-wheezy-raspbian.zip"

#Check for Root
if [ "$(id -u)" != 0 ]; then
	echo "Sorry, $0 must be run as root."
	exit 1
fi

swap="1G"
rpidir="/opt/rpi-qemu"
img_source="http://files.velocix.com/c1410/images/raspbian/2012-12-16-wheezy-raspbian/2012-12-16-wheezy-raspbian.zip"

#create rpi-dir
if [[ ! -e $rpidir ]]; then
mkdir -p $rpidir
fi

#install function
install ()
{
	apt-get update
	DEBIAN_FRONTEND=noninteractive apt-get -y \
        -o DPkg::Options::=--force-confdef \
        -o DPkg::Options::=--force-confold \
        install $@
}

install qemu qemu-system

#Download Raspbian
cd $rpidir && wget $img_source

#something here is broken
cd $rpidir && unzip 2012-12-16-wheezy-raspbian.zip
#D/L kernel
wget -O $rpidir/kernel-qemu http://xecdesign.com/downloads/linux-qemu/kernel-qemu

qemu-img create -f raw /$rpidir/swap $swap
