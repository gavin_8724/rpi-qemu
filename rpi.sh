#!/bin/bash -ex
cd /opt/rpi-qemu && qemu-system-arm -kernel kernel-qemu -cpu arm1176 -m 256 -M versatilepb -no-reboot -serial stdio -append "root=/dev/sda2 panic=1" -hda 2012-10-28-wheezy-raspbian.img -hdb swap

#Do on booted pi to make use of swap
#mkswap /dev/sdb
#swapon /dev/sdb
